<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/js/bootstrap.min.css">

    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();  ?>assets/css/app-responsive.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();  ?>assets/css/<?= $css;  ?>.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();  ?>assets/css/<?= $responsive;  ?>.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();  ?>assets/font-awesome.css">

    <link
      rel="shortcut icon"
      href="<?= base_url();  ?>assets/images/logo/lambang.png"
      type="image/x-icon"
    />

            <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script
      src="<?= base_url(); ?>assets/js/2baad1d54e.js"
    ></script>

    <link rel="stylesheet" href="<?= base_url();  ?>assets/icofont/icofont.min.css">

    <link rel="stylesheet" type="css" href="<?= base_url(); ?>assets/js/slick.min.css"/>

    <script src="<?= base_url(); ?>assets/js/sweetalert.min.js"></script>
      
    <link rel="stylesheet" href="<?= base_url(); ?>assets/select2-4.0.6-rc.1/dist/css/select2.min.css">

    <link rel="stylesheet" href="<?= base_url(); ?>assets/lightbox2-2.11.1/dist/css/lightbox.css">

  </head>
  <body>

  <div class="loading-animation-screen">
    <div class="overlay-screen"></div>
    <img src="<?= base_url(); ?>assets/images/icon/loading.gif" alt="loading.." class="img-loading">
  </div>

  <?php
  $setting = $this->db->get('settings')->row_array();
  $dateNow = date('Y-m-d H:i');
  $dateDB = $setting['promo_time'];
  $dateDBNew = str_replace("T"," ",$dateDB);
  if($dateNow >= $dateDBNew){
    $this->db->set('promo', 0);
    $this->db->update('settings');
  }
  ?>

<?php if($this->config->item('icon_wa')){ ?>
<a href="https://wa.me/<?= $this->config->item('whatsappv2') ?>" target="_blank"><img src="<?= base_url(); ?>assets/images/icon/whatsapp.svg" alt="logo-whatsapp" class="icon-whatsapp"></a>
<?php } ?>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fb941aca1d54c18d8ebfb7c/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>