<!-- <div class="navbartop">
  <a href="" class="confirm">Konfirmasi Pembayaran</a>
  <a href="">Daftar</a>
  <a href="">Masuk</a>
</div> -->
<?php if($this->session->userdata('login')){ ?>
  <?php
  $user = $this->db->get_where('user', ['id' => $this->session->userdata('id')])->row_array();
  $cart = $this->db->get_where('cart', ['user' => $this->session->userdata('id')]);
  $order = $this->db->get_where('invoice', ['user' => $this->session->userdata('id'), 'status !=' => 4]);
  ?>
<?php } ?>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark" style="background-color: <?= $this->config->item('navbar_color'); ?>">
  <div class="container">
  
    <a class="navbar-brand mr-5" href="<?= base_url(); ?>"><h3><?= $this->config->item('app_name'); ?></h3></a>


    <div class="collapse navbar-collapse ml-3" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?= base_url(); ?>">Beranda</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link text-light dropdown-toggle" href="#" id="navbarDropdownCategories" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kategori
          </a>
          <?php $categories = $this->Categories_model->getCategories(); ?>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownCategories">
            <?php foreach($categories->result_array() as $cat): ?>
              <a class="dropdown-item" href="<?= base_url(); ?>c/<?= $cat['slug']; ?>"><?= $cat['name']; ?></a>
            <?php endforeach; ?>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="<?= base_url(); ?>testimoni">Testimoni</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link text-light" href="<?= base_url(); ?>payment/confirmation">Konfirmasi Pembayaran</a>
        </li> -->
<!--         <li class="nav-item">
        <?php if ($this->session->userdata('login')) { ?>
          <a class="nav-link text-light" href="<?= base_url(); ?>payment/confirmation">Konfirmasi Pembayaran</a>
          <a class="nav-link text-light" href="<?= base_url(); ?>profile/transaction_lunas">Konfirmasi Pembayaran</a>
          <?php } else{ 

          }?>
        </li> -->
      </ul>
      <br>
      <div>

        
      </div>
      <?php if($this->session->userdata('login')){ ?>
        <a href="<?= base_url(); ?>cart" class="text-light navbar-cart-inform">
          <i class="fa fa-shopping-cart"></i>
          <?php if($cart->num_rows() > 0){ ?>
            Cart(<?= $cart->num_rows(); ?>)
          <?php }else{ ?>
            Cart
          <?php } ?>
        </a>
        <br>
        <br>
        <br>
      <?php } ?>
    </div>

    <?php if($this->session->userdata('login')){ ?>
    <div>

      <i class="fa text-light ml-3 icon-search-navbar fa-search" href="" data-toggle="modal" data-target="#modal2MoreCategory"></i>
      <img src="<?= base_url(); ?>assets/images/profile/<?= $user['photo_profile']; ?>" class="photo-profile-mobile" alt="Photo Profile <?= $user['name']; ?>" class="photo" data-toggle="dropdown" id="dropdownPhotoProfileNavbarMobile" aria-haspopup="true" aria-expanded="false">
      <div class="dropdown-menu dropdownPhotoProfileNavbarMobile" aria-labelledby="dropdownPhotoProfileNavbarMobile">

        <a class="dropdown-item" href="<?= base_url(); ?>profile">Dashboard</a>

        <a class="dropdown-item" href="" data-toggle="modal" data-target="#modal1MoreCategory">Kategori</a>

        <a class="dropdown-item" href="<?= base_url(); ?>cart">
        <?php if($cart->num_rows() > 0){ ?>
            Keranjang(<?= $cart->num_rows(); ?>)
          <?php }else{ ?>
            Keranjang
          <?php } ?>
        </a>
        <?php if($order->num_rows() > 0){ ?>
          <a class="dropdown-item" href="<?= base_url(); ?>profile/transaction">Transaksi <small class="badge badge-sm badge-info"><?= $order->num_rows(); ?></small></a>
        <?php }else{ ?>
          <a class="dropdown-item" href="<?= base_url(); ?>profile/transaction">Transaksi</a>
        <?php } ?>
        <a class="dropdown-item" href="<?= base_url(); ?>profile/histories">Riwayat Transaksi</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="<?= base_url(); ?>payment/confirmation">Konfirmasi Pembayaran</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="<?= base_url(); ?>profile/edit-profile">Edit Profil</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="<?= base_url(); ?>logout">Keluar</a>
      </div>
    </div>
    <?php }else{ ?>
      <div>

      <i class="fa text-light ml-3 icon-search-navbar fa-search" href="" data-toggle="modal" data-target="#modal2MoreCategory"></i>
        <a href="<?= base_url(); ?>login" class="btn btn-sm btn-outline-light ml-2"><i class="fa fa-sign-in-alt"></i> Masuk</a>
      </div>
    <?php } ?>

  </div>
</nav>

<!-- Modal More Category -->
<div class="modal fade" id="modal2MoreCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Cari Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div >
  <form action="<?= base_url(); ?>search" method="get">
    <input style="width: 80%" type="text" placeholder="Masukkan Nama Produk" autocomplete="off" name="q">
    <button class="btn btn-primary" style="height: auto" type="submit">Cari</button>
  </form>
</div>
      </div>
    </div>
  </div>
</div>
<!-- Modal More Category -->
<div class="modal fade" id="modal1MoreCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Semua Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="category">
          <?php foreach($categories->result_array() as $c): ?>
            <a href="<?= base_url(); ?>c/<?= $c['slug']; ?>">
              <div class="item">
                  <p><?= $c['name']; ?></p>
              </div>
            </a>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div class="search-form">
  <i class="fa fa-times"></i>
  <form action="<?= base_url(); ?>search" method="get">
    <input type="text" placeholder="Cari produk" autocomplete="off" name="q">
    <button type="submit">Cari</i></button>
  </form>
</div> -->
<div class="top-nav"></div>
