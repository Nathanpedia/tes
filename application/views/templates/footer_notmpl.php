<script src="<?= base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/slick.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script>
//loading screen
$(window).ready(function(){
    $(".loading-animation-screen").fadeOut("slow");
    $('#modalRegisterSuccess').modal('show');
})

</script>
</body>
</html>