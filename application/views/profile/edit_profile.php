<?php echo $this->session->flashdata('success'); ?>
<div class="wrapper">
    <?php include 'menu.php'; ?>
    <div class="core">
        <h2 class="title">Edit Profil</h2>
        <hr>
        <?php echo $this->session->flashdata('failed'); ?>
        <form action="<?= base_url(); ?>profile/edit-profile" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Nama Lengkap</label>
                <input type="text" name="name" value="<?= $user['name']; ?>" class="form-control" id="name" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('name'); ?></small>
            </div>
            <div class="form-group">
                <label for="address">Alamat</label>
                <input type="text" name="address" value="<?= $user['address']; ?>" class="form-control" id="address" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('address'); ?></small>
            </div>
            <div class="form-group">
                <label for="label">Label Alamat</label>
                <input type="text" name="label" value="<?= $user['label']; ?>" placeholder="Contoh: Rumah, Kantor, Kos, dll" class="form-control" id="label" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('label'); ?></small>
            </div>
            <div class="form-group">
                <label for="telp">Nomor Telfon</label>
                <input type="number" name="telp" value="<?= $user['telp']; ?>" class="form-control" id="telp" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('telp'); ?></small>
            </div>
            <div class="form-group">
                <label for="zipcode">Kode Pos</label>
                <input type="number" name="zipcode" value="<?= $user['zipcode']; ?>" class="form-control" id="zipcode" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('zipcode'); ?></small>
            </div>
            <div class="form-group">
                <label for="userSelectProvince">Provinsi</label>
                <select name="userSelectProvince" id="userSelectProvince" class="form-control" required>
                <option value="<?= $selectProvinces['province_id']; ?>"><?= $selectProvinces['province']; ?></option>
                <?php foreach($provinces as $re): ?>
                <option value="<?= $re['province_id']; ?>"><?= $re['province']; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="userSelectRegency">Kabupaten atau Kota</label>
                <select name="userSelectRegency" id="userSelectRegency" class="form-control" required>
                <option value="<?= $selectRegencys['city_id']; ?>"><?= $selectRegencys['type']; ?> <?= $selectRegencys['city_name']; ?></option>
                <?php foreach($regency as $r): ?>
                <option value="<?= $r['city_id']; ?>"><?= $r['type']; ?> <?= $r['city_name']; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="district">Kecamatan</label>
                <input type="text" name="district" value="<?= $user['district']; ?>" class="form-control" id="district" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('district'); ?></small>
            </div>
            <div class="form-group">
                <label for="village">Desa / Kelurahan</label>
                <input type="text" name="village" value="<?= $user['village']; ?>" class="form-control" id="village" required autocomplete="off">
                <small class="form-text text-danger pl-1"><?php echo form_error('village'); ?></small>
            </div>
            <div class="form-group">
                <label for="photo">Foto Profil</label><br>
                <img src="<?= base_url(); ?>assets/images/profile/<?= $user['photo_profile']; ?>" alt="Foto profil <?= $user['name']; ?>" class="photo-profile">
                <input type="file" name="newphoto" class="form-control mt-2" id="photo">
            </div>
            <button class="btn btn-dark">Update</button>
        </form>
    </div>
</div>