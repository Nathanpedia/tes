<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function getUsers($number,$offset){
        $this->db->order_by('id', 'desc');
        return $this->db->get('user',$number,$offset);
    }

    public function getProfile(){
        $id = $this->session->userdata('id');
        return $this->db->get_where('user', array('id' => $id))->row_array();
    }

    public function getOrder(){
        $id = $this->session->userdata('id');
        $this->db->where('status !=', 4);
        $this->db->where('user', $id);
        $this->db->order_by('id', 'desc');
        return $this->db->get('invoice');
    }

    public function getFinishOrder(){
        $id = $this->session->userdata('id');
        $this->db->where('status', 4);
        $this->db->where('user', $id);
        $this->db->order_by('id', 'desc');
        return $this->db->get('invoice');
    }

    public function getOrderByInvoice($id){
        $user = $this->session->userdata('id');
        return $this->db->get_where('invoice', array ('invoice_code' => $id, 'user' => $user))->row_array();
    }

    public function register(){
        $email = addslashes(htmlspecialchars($this->input->post('email', true)));
        $checkEmail = $this->db->get_where('user', array('email' => $email))->row_array();
        if($checkEmail){
            $this->session->set_flashdata('failed', '<div class="alert alert-danger" role="alert">
            Email sudah ada!
            </div>');
            redirect(base_url() . 'register');
        }else{
            $name = addslashes(htmlspecialchars($this->input->post('name', true)));
            $password = $this->input->post('password');
            $token = sha1(rand());
            function textToSlug($text='') {
                $text = trim($text);
                if (empty($text)) return '';
                $text = preg_replace("/[^a-zA-Z0-9\-\s]+/", "", $text);
                $text = strtolower(trim($text));
                $text = str_replace(' ', '-', $text);
                $text = $text_ori = preg_replace('/\-{2,}/', '-', $text);
                return $text;
            }
            $username = textToSlug($name);
            $checkUsername = $this->db->get_where('user', array('username' => $username))->row_array();
            if($checkUsername){
                $username = $username . substr(rand(),0,3);
            }
            $data = array(
                'name' => $name,
                'username' => $username,
                'email' => $email,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'date_register' => date('Y-m-d H:i:s'),
                'token' => $token,
                'photo_profile' => 'default.png'
            );
            $this->db->insert('user', $data);

            $data = array(
                'email' => $email,
                'date_subs' => date('Y-m-d H:i:s'),
                'code' => time() . rand()
            );
            $this->db->insert('subscriber', $data);

            $this->load->library('email');
            $config['charset'] = 'utf-8';
            $config['useragent'] = $this->config->item('app_name');
            $config['smtp_crypto'] = $this->config->item('smtp_crypto');
            $config['protocol'] = 'smtp';
            $config['mailtype'] = 'html';
            $config['smtp_host'] = $this->config->item('host_mail');
            $config['smtp_port'] = $this->config->item('port_mail');
            $config['smtp_timeout'] = '5';
            $config['smtp_user'] = $this->config->item('mail_account');
            $config['smtp_pass'] = $this->config->item('pass_mail');
            $config['crlf'] = "\r\n";
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
            $this->email->to($email);
            $this->email->subject('Verifikasi address Email '.$this->config->item("app_name"));
            $this->email->message(
                '<p><strong>Halo '.$name.'</strong><br>
                Terima kasih telah mendaftar di '.$this->config->item('app_name').'. <br/>
                Silakan verifikasi email dengan klik link dibawah ini: <br/>
                <a href="'.base_url().'auth/verification?email='.$email.'&token='.$token.'">'.base_url().'auth/verification?email='.$email.'&token='.$token.'</a><br/>
                Terima kasih</p>
                ');
            $this->email->send();
        }
    }

    public function getProductByInvoice($id){
        $user = $this->session->userdata('id');
        return $this->db->get_where('transaction', array('user' => $user, 'id_invoice' => $id));
    }

    public function uploadPhoto(){
        $config['upload_path'] = './assets/images/profile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '2048';
        $config['file_name'] = round(microtime(true)*1000);

        $this->load->library('upload', $config);
        if($this->upload->do_upload('newphoto')){
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    public function getRegency(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/city",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }

    public function getRegencyById(){
      $getDB = $this->db->get('user')->row_array();
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/city?id=".$getDB['regency_id'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }

    public function getProvinces(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/province?id=".$getDB['province_id'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }

    public function getProvincesById(){
      $getDB = $this->db->get('user')->row_array();
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/province?id=".$getDB['province_id'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }

    public function updateProfile($file){
        if($file == ""){
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $label = $this->input->post('label');
            $telp = $this->input->post('telp');
            $zipcode = $this->input->post('zipcode');
            $district = $this->input->post('district');
            $village = $this->input->post('village');
            $regency = $this->input->post('userSelectRegency', true);
            $province = $this->input->post('userSelectProvince', true);
            $data = [
                    'regency_id' => $regency,
                    'province_id' => $province
                    ];
            
            $this->db->set('name', $name);
            $this->db->set('address', $address);
            $this->db->set('label', $label);
            $this->db->set('telp', $telp);
            $this->db->set('zipcode', $zipcode);
            $this->db->set('district', $district);
            $this->db->set('village', $village);
            $this->db->where('id', $this->session->userdata('id'));
            $this->db->update('user');
            $this->db->update('user', $data);
            $this->db->update('address');
            $this->db->update('label');
            $this->db->update('telp');
            $this->db->update('zipcodecode');
            $this->db->update('district');
            $this->db->update('village');
            
        }else{
            $name = $this->input->post('name');
            $this->db->set('name', $name);
            
            $address = $this->input->post('address');
            $this->db->set('address', $address);

            $label = $this->input->post('label');
            $this->db->set('label', $label);

            $telp = $this->input->post('telp');
            $this->db->set('telp', $telp);

            $zipcode = $this->input->post('zipcode');
            $this->db->set('zipcode', $zipcode);

            $regency = $this->input->post('userSelectRegency', true);
            $province = $this->input->post('userSelectProvince', true);
            $data = [
                    'regency_id' => $regency,
                    'province_id' => $province
                    ];
            $this->db->update('user', $data);

            $district = $this->input->post('district');
            $this->db->set('district', $district);

            $this->db->set('photo_profile', $file);
            $this->db->where('id', $this->session->userdata('id'));
            $this->db->update('user');
        }
    }

}